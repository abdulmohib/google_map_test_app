// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:flutter/services.dart';

// class MapScreen extends StatefulWidget {
//   const MapScreen({super.key});

//   @override
//   _MapScreenState createState() => _MapScreenState();
// }

// class _MapScreenState extends State<MapScreen> {
//   Completer<GoogleMapController> _controller = Completer();
//   static const LatLng _center = const LatLng(45.521563, -122.677433);
//   final Set<Marker> _markers = {};
//   LatLng _lastMapPosition = _center;
//   MapType _currentMapType = MapType.normal;

//   void _onMapTypeButtonPressed() {
//     setState(() {
//       _currentMapType = _currentMapType == MapType.normal
//           ? MapType.satellite
//           : MapType.normal;
//     });
//   }

//   void _onAddMarkerButtonPressed() {
//     setState(() {
//       _markers.add(Marker(
//         markerId: MarkerId(_lastMapPosition.toString()),
//         position: _lastMapPosition,
//         infoWindow: InfoWindow(
//           title: 'This is a title',
//           snippet: 'This is a snippet',
//         ),
//         icon: BitmapDescriptor.defaultMarker,
//       ));
//     });
//   }

//   void _onCameraMove(CameraPosition position) {
//     _lastMapPosition = position.target;
//   }

//   void _onMapCreated(GoogleMapController controller) {
//     _controller.complete(controller);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Map Screen'),
//       ),
//       body: Stack(
//         children: <Widget>[
//           GoogleMap(
//             onMapCreated: _onMapCreated,
//             initialCameraPosition: CameraPosition(
//               target: _center,
//               zoom: 11.0,
//             ),
//             mapType: _currentMapType,
//             markers: _markers,
//             onCameraMove: _onCameraMove,
//           ),
//           Padding(
//             padding: const EdgeInsets.all(16.0),
//             child: Align(
//               alignment: Alignment.topRight,
//               child: Column(
//                 children: <Widget>[
//                   FloatingActionButton(
//                     onPressed: _onMapTypeButtonPressed,
//                     materialTapTargetSize: MaterialTapTargetSize.padded,
//                     backgroundColor: Colors.green,
//                     child: const Icon(Icons.map, size: 36.0),
//                   ),
//                   SizedBox(height: 16.0),
//                   FloatingActionButton(
//                     onPressed: _onAddMarkerButtonPressed,
//                     materialTapTargetSize: MaterialTapTargetSize.padded,
//                     backgroundColor: Colors.green,
//                     child: const Icon(Icons.add_location, size: 36.0),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MapInWebviewScreen extends StatefulWidget {
  const MapInWebviewScreen({super.key});

  @override
  _MapInWebviewScreenState createState() => _MapInWebviewScreenState();
}

class _MapInWebviewScreenState extends State<MapInWebviewScreen> {
  final _controller = WebViewController()
    ..setJavaScriptMode(JavaScriptMode.unrestricted)
    ..setBackgroundColor(const Color(0x00000000))
    ..setNavigationDelegate(
      NavigationDelegate(
        onProgress: (int progress) {
          // Update loading bar.
        },
        onPageStarted: (String url) {},
        onPageFinished: (String url) {},
        onWebResourceError: (WebResourceError error) {},
        onNavigationRequest: (NavigationRequest request) {
          return NavigationDecision.navigate;
        },
      ),
    )
    ..loadRequest(
        Uri.parse('https://www.google.com/maps/@45.521563,-122.677433,14z'));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Google Map'),
      ),
      body: WebViewWidget(
        controller: _controller,
      ),
    );
  }
}

