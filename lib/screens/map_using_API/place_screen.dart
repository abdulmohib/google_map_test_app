import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';

const kGoogleApiKey = "YOUR_API_KEY"; // add your API key here

class PlaceDetailScreen extends StatefulWidget {
  final String? placeId;

  const PlaceDetailScreen({super.key, this.placeId});

  @override
  _PlaceDetailScreenState createState() => _PlaceDetailScreenState();
}

class _PlaceDetailScreenState extends State<PlaceDetailScreen> {
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
  late PlaceDetails _placeDetails;

  @override
  void initState() {
    super.initState();
    _getPlaceDetails();
  }

  void _getPlaceDetails() async {
    final result = await _places.getDetailsByPlaceId(widget.placeId);
    setState(() {
      _placeDetails = result as PlaceDetails;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Place Detail'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            height: 300,
            width: double.infinity,
            child: _placeDetails == null
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(
                    itemCount: _placeDetails.photos.length,
                    itemBuilder: (context, index) {
                      return Container(
                        height: 200,
                        width: double.infinity,
                        child: Image.network(
                            _placeDetails.photos[index].photoReference),
                      );
                    },
                  ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Text(
              _placeDetails == null ? '' : _placeDetails.name,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Text(
              _placeDetails == null ? '' : _placeDetails.formattedAddress,
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MapWithSearchScreen extends StatefulWidget {
  const MapWithSearchScreen({super.key});

  @override
  _MapWithSearchScreenState createState() => _MapWithSearchScreenState();
}

class _MapWithSearchScreenState extends State<MapWithSearchScreen> {
  Completer<GoogleMapController> _controller = Completer();
  static const LatLng _center = const LatLng(45.521563, -122.677433);
  final Set<Marker> _markers = {};
  String _query = 'restaurant';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Map with Search'),
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: const CameraPosition(
              target: _center,
              zoom: 14.0,
            ),
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
              _searchNearby(_query);
            },
            markers: _markers,
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Search...',
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left: 15, top: 15),
                  suffixIcon: IconButton(
                    icon: Icon(Icons.search),
                    onPressed: () {
                      _searchNearby(_query);
                    },
                  ),
                ),
                onChanged: (value) {
                  setState(() {
                    _query = value;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _searchNearby(String query) async {
    final GoogleMapController controller = await _controller.future;
    const cameraPosition = CameraPosition(
      target: _center,
      zoom: 14.0,
    );
    Location _loc = Location(45.521563, -122.677433);
    controller.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    final GoogleMapsPlaces places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
    final result = await places.searchNearbyWithRadius(_loc, 5000, type: query);

    setState(() {
      _markers.clear();
      for (var f in result.results) {
        _markers.add(
          Marker(
            markerId: MarkerId(f.placeId),
            position: LatLng(f.geometry.location.lat, f.geometry.location.lng),
            infoWindow: InfoWindow(
              title: f.name,
              snippet: f.formattedAddress,
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => PlaceDetailScreen(
                    placeId: f.placeId,
                  ),
                ),
              );
            },
          ),
        );
      }
    });
  }
}
